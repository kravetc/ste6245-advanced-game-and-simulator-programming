#ifndef PLANE_H
#define PLANE_H

#include <parametrics/gmpplane>

class Plane: public GMlib::PPlane<float>{
public:
    Plane(GMlib::Point<float,3> p, GMlib::Vector<float,3> v1, GMlib::Vector<float,3> v2);
    ~Plane();

    GMlib::Point<float, 3> getCornerPoint();
    GMlib::Vector<float,3> normal();

private:
    GMlib::Vector<float,3> v1;
    GMlib::Vector<float,3> v2;


protected:
void localSimulate(double dt){}
};

#endif
