#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>

#include "plane.h"
#include "ball.h"
#include "player.h"
#include "collobj.h"

#include "core/gmarray"

#define EPS 1e-8

class Controller:public GMlib::PSphere<float> {
GM_SCENEOBJECT(Controller)

private:

GMlib::Array <Ball*> BallsArray;
GMlib::Array <Plane*> PlaneArray;
GMlib::Array<Player*> PlayerArray;
GMlib::Array <CollObj> Collision;
GMlib::PBezierSurf<float>* surf;

double _dt;

GMlib::Point<float,2> out;

bool rayCastPlane(GMlib::Point<float, 2> rayOrigin, GMlib::Vector<float, 2> rayDirection, GMlib::Point<float, 2> planeOrigin, GMlib::Vector<float, 2> planeNormal);
bool isInsideSimplex(GMlib::Point<float, 2> point, Player *w);


public:
Controller();
Controller(GMlib::PBezierSurf<float> *p);
void insertBall(Ball *b);
void insertPlane(Plane *p);
void insertPlayer(Player* pl);

void removeBall(Ball *b);

protected:

void findBBcoll(Ball *b1, Ball *b2, GMlib::Array<CollObj> &co, float y);
void findBWcoll(Ball *b, Plane *p, GMlib::Array<CollObj> &co, float y);
void findBPcoll(Ball *b, Player* pl, GMlib::Array<CollObj> &co, float y);
void localSimulate(double dt);
void colBB(Ball *b1, Ball *b2, double dt);
void colBW(Ball *b, Plane *p, double dt);
void colBP(Ball *b, Player *pl, double dt);

};

#endif
