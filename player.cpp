#include "player.h"

Player::Player(GMlib::Point<float, 2> begin, GMlib::Point<float, 2> end, GMlib::Vector<float, 2> n): GMlib::PPlane<float>(GMlib::Point<float,3>{begin[0], begin[1], 0.}, GMlib::Vector<float,3>{(end[0] - begin[0]), (end[1] - begin[1]), 0.}, GMlib::Vector<float,3>{0., 0., 3.}){

    this->begin = begin;
    this->end = end;
    this->norm = n;
}

Player::~Player(){}

GMlib::Vector<float,2> Player::normal(){
    return norm;
}

GMlib::Point<float,2> Player::getBegin(){
    return begin;
}

GMlib::Point<float,2> Player::getEnd(){
    return end;
}


void Player::moveLeft(){
    this->translate({-1., 0., 0.});
    this->begin[0] += -1.0;
    this->begin[1] += 0.0;
    this->end[0] += -1.0;
    this->end[1] += 0.0;
}

void Player::moveRight(){
    this->translate({1., 0., 0.});
    this->begin[0] += 1.0;
    this->begin[1] += 0.0;
    this->end[0] += 1.0;
    this->end[1] += 0.0;
}


