	
#ifndef COLLOBJ_H
#define COLLOBJ_H

#include "plane.h"
#include "ball.h"
#include "player.h"

class CollObj {

private:
Ball* b[2];
Plane* p;
Player* pl;
double t;
bool bw;

int type;

public:
CollObj(Ball* b1, Ball* b2, double t){
    b[0] = b1;
    b[1] = b2;
    this->t = t;
    bw = false;
  }

CollObj(Ball* b1, Plane* p, double t){
    b[0] = b1;
    this->p = p;
    this->t = t;
    bw = true;
    type = 1;
  }


CollObj(Ball* b1, Player* pl, double t){
    b[0] = b1;
    this->pl = pl;
    this->t = t;
    bw = true;
    type = 2;
  }

CollObj(){}

bool operator < ( const CollObj &other) const {
  return t < other.t;
}

bool operator == (const CollObj &other) const{
  if (b[0] == other.b[0]) return true;
  if (!other.bw && b[0] == other.b[1]) return true;
  if (!bw && b[1] == other.b[0]) return true;
  if (!bw && !other.bw && b[1] == other.b[1]) return true;

  return false;
} 

Ball* getBall(int i){
  return b[i];
}

Plane* getPlane(){
  return p;
}


Player* getPlayer(){
    return pl;
}

double getTime(){
  return t;
}

void setTime(double time){
  t = time;
}

bool getBW(){
return bw;
}

int getType(){
    return type;
}
};


#endif // COLLOBJ_H
