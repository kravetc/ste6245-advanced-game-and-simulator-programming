#include "controller.h" 

Controller::Controller(){
    this->toggleVisible();
}

Controller::Controller(GMlib::PBezierSurf<float> *p){

this->insert(p);
this->surf = p;

}


void Controller::insertBall(Ball *b){

this->insert(b);
this->BallsArray += b;

b->setUV(this->surf);

}

void Controller::insertPlane(Plane *p){

this->insert(p);
this->PlaneArray += p;

}


void Controller::insertPlayer(Player* pl){

this->insert(pl);
this->PlayerArray += pl;

}

void Controller::removeBall(Ball *b){
    this->remove(b);
    this->BallsArray.remove(b);
}

bool Controller::isInsideSimplex(GMlib::Point<float, 2> point, Player *w){
    GMlib::Vector<float, 2> p0p = point - w->getBegin(); //ac
    GMlib::Vector<float, 2> p1p = point - w->getEnd(); //cb
    GMlib::Vector<float, 2> p0p1 = w->getBegin() - w->getEnd(); //ab
    if (p0p.getLength() + p1p.getLength() == p0p1.getLength()) //if ac+cb=ab
        return true;
    else
        return false;
}

bool Controller::rayCastPlane(GMlib::Point<float,2> rayOrigin, GMlib::Vector<float,2> rayDirection, GMlib::Point<float, 2> planeOrigin, GMlib::Vector<float, 2> planeNormal){
    double x;

    if (abs(rayDirection*planeNormal) < EPS) //rayDirection is perpendicular to normal
        return false;

    x =  -((rayOrigin - planeOrigin)*planeNormal)/(rayDirection*planeNormal);

    if (x >= 0){
        out = rayOrigin + x*rayDirection;
        return true;
    }
    return false;
}


void Controller::localSimulate(double dt) {

for (int i = 0; i < BallsArray.size(); i++ )
    BallsArray[i]->update(dt);

//detection all collisions
for (int i = 0; i < BallsArray.size(); i++ )
  for (int j = i+1; j < BallsArray.size(); j++)
    findBBcoll(BallsArray[i], BallsArray[j], Collision, 0);

for (int i = 0; i < BallsArray.size(); i++ )
  for (int j = 0; j < PlaneArray.size(); j++)
    findBWcoll(BallsArray[i], PlaneArray[j], Collision, 0);

for (int i = 0; i < BallsArray.size(); i++ )
   for (int j = 0; j < PlayerArray.size(); j++)
        findBPcoll(BallsArray[i], PlayerArray[j], Collision, 0);

while(Collision.getSize() > 0){

    Collision.sort();
    Collision.makeUnique();

    CollObj co = Collision[0];
    Collision.removeFront();

    if(co.getBW()){ //if collision with plane
        if (co.getType() == 1){ //if plane
            colBW(co.getBall(0), co.getPlane(), (1-co.getTime())*dt);

            co.getBall(0)->setTime(co.getTime());

            for (int i = 0; i < BallsArray.size(); i++ )
              if(BallsArray[i] != co.getBall(0))
                findBBcoll(co.getBall(0), BallsArray[i], Collision, co.getTime());

            for (int i = 0; i < PlaneArray.size(); i++ )
              if(PlaneArray[i] != co.getPlane())
                findBWcoll(co.getBall(0), PlaneArray[i], Collision, co.getTime());
        }
        if (co.getType()  == 2){ //if section of plane
            colBP(co.getBall(0), co.getPlayer(), (1-co.getTime())*dt);

            co.getBall(0)->setTime(co.getTime());

            for (int i = 0; i < BallsArray.size(); i++ )
               if(BallsArray[i] != co.getBall(0))
                    findBBcoll(co.getBall(0), BallsArray[i], Collision, co.getTime());

            for (int i = 0; i < PlayerArray.size(); i++ )
                if(PlayerArray[i] != co.getPlayer())
                      findBPcoll(co.getBall(0), PlayerArray[i], Collision, co.getTime());
        }
      }
    else { //if collision between balls
        co.getBall(0)->setTime(co.getTime());
        co.getBall(1)->setTime(co.getTime());

        colBB(co.getBall(0), co.getBall(1), (1-co.getTime())*dt);

        for (int i = 0; i < PlaneArray.size(); i++ )
        {
            findBWcoll( co.getBall(0), PlaneArray[i], Collision, co.getTime());
            findBWcoll( co.getBall(1), PlaneArray[i], Collision, co.getTime());

        }

        for (int i = 0; i < PlayerArray.size(); i++ ){
            findBPcoll(co.getBall(0), PlayerArray[i], Collision, co.getTime());
            findBPcoll(co.getBall(1), PlayerArray[i], Collision, co.getTime());
        }

        for (int i = 0; i < BallsArray.size(); i++ )
            if(BallsArray[i] != co.getBall(0) && BallsArray[i] != co.getBall(1))
            {

                findBBcoll(co.getBall(0), BallsArray[i], Collision, co.getTime());
                findBBcoll(co.getBall(1), BallsArray[i], Collision, co.getTime());

            }
    }

    for (int i = 0; i < BallsArray.size(); i++ )
        if(BallsArray[i]->getPos()(1) <= -8.9){
            std::cout << "player1 lose" <<  std::endl;
            removeBall(BallsArray[i]);
        }

    for (int i = 0; i < BallsArray.size(); i++ )
        if(BallsArray[i]->getPos()(1) >= 8.9){
            std::cout << "player2 lose" <<  std::endl;
            removeBall(BallsArray[i]);
        }

}

}



void Controller::findBBcoll(Ball *b1, Ball *b2, GMlib::Array<CollObj> &co, float y){

    GMlib::Vector<float,3> k = b1->getDs() - b2->getDs();
    GMlib::Vector<float,3> q = b1->getPos() - b2->getPos();
    float r = b1->getRadius() + b2->getRadius();

    float a = k*k;
    float b = 2*q*k;
    float c = q*q - r*r;
    float d = b*b - 4*a*c;

    if (c < 0){ //if balls are intersect
        float t = 0.51*(r - q.getLength())/q.getLength();
        b1->translate(t*q);
        b2->translate(-t*q);

        q *= 1 + 2*t;
        b = q*k;
        c = q*q - r*r;
    }

    double t = (-b - sqrt(d))/(2 * a); //smallest x

    if(t > y && t <= 1.)
      Collision.insertAlways(CollObj(b1,b2,t),true);
}

void Controller::findBWcoll(Ball *b, Plane *p, GMlib::Array<CollObj> &co, float y){

GMlib::Vector<float,3> d = p->getCornerPoint() - b->getPos();

float dsn = b->getDs()*p->getNormal();
float dn = d*p->getNormal();

if((dn + b->getRadius()) > 0.){ //if ball and plane are intersect
    b->translate(2.*(dn + b->getRadius())*p->getNormal());
    dn -= 2.*(dn + b->getRadius());
}

if (dsn < -EPS){ //angle between plane normal and ds > 90
    float t = (b->getRadius() + dn)/dsn;

    if(t > y && t <= 1.)
        Collision.insertAlways(CollObj(b,p,t),true);

}

}



void Controller::findBPcoll(Ball *b, Player* pl, GMlib::Array<CollObj> &co, float y){
    GMlib::Point<float,2> point;
    GMlib::Vector<float,2> normal = pl->normal();

    GMlib::Vector<float, 2> prVel = {b->getVel()[0], b->getVel()[1]};
    GMlib::Point<float, 2> prPos = {b->getP()[0], b->getP()[1]};

    double time = 0.;
    double t1, t2;

    GMlib::Point<float,2> planeOrigin1 = pl->getBegin() + normal*b->getRadius();

    bool collision = false;

    GMlib::Point<float,3> cp = {pl->getBegin()[0], pl->getBegin()[1], b->getRadius()};
    GMlib::Vector<float,3> no = {pl->normal()[0], pl->normal()[1], 0.0};
    GMlib::Point<float,3> po = b->getPos();
    GMlib::Vector<float,3> d = cp - po;

    //check only one side
    if (rayCastPlane(prPos, prVel, planeOrigin1, normal)){ //(1) if ds intersect with plane
        point = out;
        GMlib::Point<float,2> point1 = point - normal*b->getRadius();
        if (isInsideSimplex(point1, pl)){ //if point belongs to the interval
            float dsn = b->getDs()*no;
            float dn = d*no;

            if((dn + b->getRadius()) > 0.){
                b->translate(2.*(dn + b->getRadius())*no);
                dn -= 2.*(dn + b->getRadius());
            }
            if (dsn < -EPS){
                time = (b->getRadius() + dn)/dsn;
                t1 = ((point1 - prPos).getLength())/(prVel.getLength());
                if(!collision) time = t1;
                else time = std::min(time, t1);
                collision = true;

            }
            if(time > y && time <= 0.2 && collision)
                  Collision.insertAlways(CollObj(b,pl,time),true);
        }
    }
    else{ //(2) if collide with a or b point

        GMlib::Point<float,3> vertex1 = {pl->getBegin()[0], pl->getBegin()[1], b->getRadius()};
        GMlib::Point<float,3> vertex2 = {pl->getEnd()[0], pl->getEnd()[1], b->getRadius()};
        GMlib::Vector<float,3> v1speed = {-1., 0., 0.};
        GMlib::Vector<float,3> v2speed = {1., 0., 0.};

        if((vertex1 - b->getPos()).getLength() - b->getRadius() < EPS){ //if distance between ball position and vertex1 is ball radius

            GMlib::Vector<float,3> k = b->getDs() - v1speed;
            GMlib::Vector<float,3> q = b->getPos() - vertex1;
            float r = b->getRadius();

            float a = k*k;
            float b1 = 2*q*k;
            float c = q*q - r*r;
            float d = b1*b1 - 4*a*c;

            t2 = fabs((-b1 - sqrt(d))/(2 * a));

            if(!collision) time = t2;
            else time = std::min(time, t2);
            collision = true;

            if(time > y && time <= 1. && collision)
                  Collision.insertAlways(CollObj(b,pl,time),true);
        }
        if((vertex2 - b->getPos()).getLength() - b->getRadius() < EPS){ //if distance between ball position and vertex2 is ball radius

            GMlib::Vector<float,3> k = b->getDs() - v2speed;
            GMlib::Vector<float,3> q = b->getPos() - vertex2;
            float r = b->getRadius();

            float a = k*k;
            float b1 = 2*q*k;
            float c = q*q - r*r;
            float d = b1*b1 - 4*a*c;

            t2 = fabs((-b1 - sqrt(d))/(2 * a));

            if(!collision) time = t2;
            else time = std::min(time, t2);
            collision = true;

            if(time > y && time <= 1. && collision)
                  Collision.insertAlways(CollObj(b,pl,time),true);

        }
    }
}


void Controller::colBB(Ball *b1, Ball *b2, double dt){

    GMlib::Vector<float,3> u1 = b1->getVel();
    GMlib::Vector<float,3> u2 = b2->getVel();
    float m1 = b1->getMass();
    float m2 = b2->getMass();


    GMlib::Vector<float,3> X_Axis = b2->getPos() - b1->getPos(); //axis X
    X_Axis.normalize();

    //projections on X
    float u1x = u1*X_Axis;
    float u2x = u2*X_Axis;

    float v1x = (u1x*m1 + u2x*m2 - (u1x - u2x)*m2)/(m1 + m2);
    float v2x = (u1x*m1 + u2x*m2 - (u2x - u1x)*m1)/(m1 + m2);

    b1->setVel(u1 + (v1x*X_Axis - u1x*X_Axis)*0.8); //0.8 - coefficient, which decreased velocity
    b1->update(dt);

    b2->setVel(u2 + (v2x*X_Axis - u2x*X_Axis)*0.8);
    b2->update(dt);

}


void Controller::colBW(Ball *b, Plane *p, double dt){
    float div = p->normal() * b->getVel();
    GMlib::Vector<float, 3> u = b->getVel();

    u -= 1.8*div*p->normal(); //reflection of the velocity vector, 1.8 - coefficient, which decreased speed

    b->setVel(u);
    b->update(dt);
}


void Controller::colBP(Ball *b, Player *pl, double dt){
    GMlib::Vector<float,3> norm = {pl->normal()[0], pl->normal()[1], 0};
    float div = norm * b->getVel();
    b->setVel(b->getVel() - 2.0*div*norm);
    b->update(dt);
}
