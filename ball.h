

#ifndef BALL_H
#define BALL_H

#include <parametrics/gmpsphere>
#include <parametrics/gmpbeziersurf>
#include <iostream>
#include "plane.h"

#define EPS 1e-8


class Ball : public GMlib::PSphere<float> {

public:

Ball(GMlib::Vector<float,3> velocity, float radius, float mass, GMlib::PBezierSurf<float>* surf);
~Ball();

GMlib::Vector<float,3> getDs();
GMlib::Vector<float,3> getP();
GMlib::Vector<float,3> getVel();
float getTime();
void setTime(float time);

float getMass();
float getRadius();

void setVel(GMlib::Vector<float,3> v);

void setUV(GMlib::PBezierSurf<float>* s);

void update(double dt);

protected:
void localSimulate(double dt);

private:

float radius;
float mass;

GMlib::PBezierSurf<float>* surf;

GMlib::Point<float,3> p;
GMlib::Point<float,3> q;

GMlib::Vector<float,3> ds;

GMlib::Vector<float,3> velocity;

float t;

GMlib::Vector<float,3> axe;

float _u;
float _v;

GMlib::Vector<float,3> normal;





};

#endif



