#include "plane.h"


Plane::Plane(GMlib::Point<float, 3> p, GMlib::Vector<float, 3> v1, GMlib::Vector<float, 3> v2): GMlib::PPlane<float>(p, v1, v2){
    this->v1 = v1;
    this->v2 = v2;

}

Plane::~Plane(){}

GMlib::Point<float,3> Plane::getCornerPoint(){
    return this->_pt;
}

GMlib::Vector<float,3> Plane::normal(){
    GMlib::Vector<float,3> n = v1^v2;
    return n.normalize();
}

