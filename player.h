#ifndef PLAYER_H
#define PLAYER_H

#include <parametrics/gmpplane>

class Player: public GMlib::PPlane<float>{
public:
    Player(GMlib::Point<float,2> begin, GMlib::Point<float,2> end, GMlib::Vector<float,2> n);
    ~Player();

    GMlib::Vector<float,2> normal();

    GMlib::Point<float,2> getBegin();
    GMlib::Point<float,2> getEnd();

    void moveLeft();
    void moveRight();

private:

    GMlib::Point<float,2> begin;
    GMlib::Point<float,2> end;

    GMlib::Vector<float, 2> norm;

protected:
void localSimulate(double dt){}
};

#endif
