#include "ball.h"

Ball::Ball(GMlib::Vector<float, 3> velocity, float radius, float mass, GMlib::PBezierSurf<float> *surf): GMlib::PSphere<float>(radius){
    this->velocity = velocity;
    this->surf = surf;
    this->mass = mass;
    this->radius = radius;
    this->t = 0;
}

Ball::~Ball(){}

GMlib::Vector<float,3> Ball::getP(){
    return p;
}

GMlib::Vector<float,3> Ball::getDs(){
    return ds;
}

GMlib::Vector<float,3> Ball::getVel(){
    return velocity;
}

float Ball::getMass(){
    return mass;
}

float Ball::getRadius(){
    return radius;
}

void Ball::setVel(GMlib::Vector<float,3> v){
    velocity = v;
}

void Ball::update(double dt) {
        const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);

        ds = dt*velocity + (0.5*dt*dt)*g;

        p = this->getPos() + ds; //point after displacement

        surf->getClosestPoint(p, _u, _v); //closest point on the surface

        GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(_u, _v, 1, 1);

        normal = m[0][1]^m[1][0]; //p_u^p_v
        normal.normalize();

        GMlib::Vector<float,3> newPos = m[0][0] + radius*normal; //point on the surface + radius = new position

        ds = newPos - this->getPos();

        //new velocity
        float v1 = velocity*velocity + 2.*(g*ds);
        velocity += g*dt;
        velocity -= (velocity*normal)*normal;

        double v2 = velocity*velocity;

        if(v2 > 0.0001)
        {
            if(v1 > 0.0001)
            velocity *= std::sqrt(v1/v2);
        }

        //axis of rotation
        axe = normal^ds;


    }

void Ball::localSimulate(double dt){
    this->translateParent(ds);
    rotateParent(GMlib::Angle((ds.getLength())/this->getRadius()), axe ); //angle of rotation = |ds|/r

}

void Ball::setUV(GMlib::PBezierSurf<float>* s){
    s->estimateClpPar(this->getPos(), _u, _v);
}

float Ball::getTime(){
    return t;
}

void Ball::setTime(float time){
    t = time;
}





